import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import scheduler.CameraDiscoveryJob;

public class SchedulerApplication {

    public static final String cronExpression = "0 0/1 * * * ?";

    public static void main(String[] args) throws Exception {

        SchedulerFactory schedulerFactory=new StdSchedulerFactory();
        Scheduler scheduler= schedulerFactory.getScheduler();
        JobDetail jobDetail= JobBuilder.newJob(CameraDiscoveryJob.class).withIdentity("cameraDiscoveryJob","group1").build();
        CronTrigger trigger=TriggerBuilder.newTrigger()
                                            .withIdentity("trigger1","group1")
                                            .withSchedule(CronScheduleBuilder.cronSchedule(cronExpression))
                                            .forJob("cameraDiscoveryJob","group1")
                                            .build();
        scheduler.scheduleJob(jobDetail,trigger);
        scheduler.start();
    }
}
