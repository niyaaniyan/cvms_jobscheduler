package scheduler;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CameraDiscoveryJob implements Job {

    public static final String discoveryServiceEndpoint="http://localhost:9095/v1/camera/update";
    private final Logger log = LoggerFactory.getLogger(CameraDiscoveryJob.class);

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        try {
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpGet request = new HttpGet(discoveryServiceEndpoint);
            HttpResponse response = httpClient.execute(request);
            if (response.getStatusLine().getStatusCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatusLine().getStatusCode());
            }
            log.info("CameraDiscoveryJob executed!");
            httpClient.getConnectionManager().shutdown();
        } catch (IOException e) {
            log.error("CameraDiscoveryJob : IOException occurred in Job execution : "+e.getMessage());
            e.printStackTrace();
        }
        catch (Exception e){
            log.error("CameraDiscoveryJob : Exception occurred in Job execution : "+e.getMessage());
            log.error("Refiring job");
            JobExecutionException e2 =
                    new JobExecutionException(e);
            e2.refireImmediately();
            throw e2;
        }
    }

}
